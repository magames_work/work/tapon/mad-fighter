﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private UIManager ui;
    private LvlManager lvl;

    public GameObject Player;
    public bool isFighting;
    public bool isWin;
    public Camera enemyCam;
    public GameObject wall;

    void Start()
    {
        ui = GetComponent<UIManager>();
        lvl = GetComponent<LvlManager>();        
    }

    void Update()
    {
        
    }

    public void loadLvl(int id)
    {
        SceneManager.LoadScene(id);
    }
}
