﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimManager : MonoBehaviour
{
    private UIManager ui;
    private LvlManager lvl;
    private GameManager gm;
    private Enemy enemy;
    private FightController fc;

    void Start()
    {
        lvl = GetComponent<LvlManager>();
        ui = GetComponent<UIManager>();
        gm = GetComponent<GameManager>();
        enemy = lvl.currentTarget.GetComponent<Enemy>();
        fc = gm.GetComponent<FightController>();
    }

    void Update()
    {
        if(fc.isPunching)
        {
            gm.Player.GetComponent<Animator>().SetBool("isFighting", true);
        }
        else
        {
           
            gm.Player.GetComponent<Animator>().SetBool("isFighting", false);
            //lvl.currentTarget.GetComponent<Animator>().SetBool("lockImpact", false);
            gm.GetComponent<FightController>().getHit = false;

        }

        if (fc.isSuperPunching)
        {
            gm.Player.GetComponent<Animator>().SetTrigger("isSuperPunch");
            fc.isSuperPunching = false;
        }

        if (fc.isEvading)
        {
            if(!enemy.isPunching)
            {
                fc.isEvaded = true;
                lvl.currentTarget.GetComponent<Animator>().SetTrigger("isPunch");
            }

            lvl.currentTarget.GetComponent<Animator>().SetBool("lockImpact", false);
           
            gm.Player.GetComponent<Animator>().SetBool("isFighting", false);
            gm.Player.GetComponent<Animator>().SetTrigger("isEvading");
            fc.isEvading = false;
           
        }
    }

    public void Impact(int id)
    {
        switch (id)
        {
            case 1:
                lvl.currentTarget.GetComponent<Animator>().SetTrigger("isFront");
                break;

            case 2:
                lvl.currentTarget.GetComponent<Animator>().SetTrigger("isLeft");
                break;

            case 3:
                lvl.currentTarget.GetComponent<Animator>().SetTrigger("isRight");
                break;

            case 4:
                lvl.currentTarget.GetComponent<Animator>().SetTrigger("isUpper");
                break;

            case 5:
                lvl.currentTarget.GetComponent<Animator>().SetTrigger("isLower");
                break;

            case 6:
                lvl.currentTarget.GetComponent<Animator>().SetBool("lockImpact", false);
                ui.alert.gameObject.SetActive(false);
                Time.timeScale = 1;
                lvl.currentTarget.GetComponent<Enemy>().isPunching = false;

                if(!gm.GetComponent<FightController>().isEvaded)
                {
                    gm.Player.GetComponent<Animator>().SetBool("isFighting", false);
                    gm.Player.GetComponent<Animator>().SetTrigger("impact");
                    gm.GetComponent<FightController>().getHit = true;
                }
                break;

            case 7:
                lvl.currentTarget.GetComponent<Animator>().SetTrigger("KO");
                break;

        }
    }
}
