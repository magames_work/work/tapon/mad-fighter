﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float health;
    public float timeToPunch, alertTime;
    public float punchDmg;

    private UIManager ui;
    private LvlManager lvl;
    private GameManager gm;
    private AnimManager anim;

    private float catchTime;
    [HideInInspector]
    public bool isPunching;
    private bool isDead;
    



    void Start()
    {
        gm = FindObjectOfType<GameManager>();     
        lvl = gm.GetComponent<LvlManager>();
        ui = gm.GetComponent<UIManager>();
        anim = gm.GetComponent<AnimManager>(); 
        catchTime = Time.time;
        isDead = false;
    }

    void Update()
    {
        if(gm.isFighting)
            Punching();

        if(health <= 0)
        {
            KOevent();
        }
    }

    void Punching()
    {
        if(catchTime + timeToPunch - alertTime < Time.time)
        {
            ui.alert.gameObject.SetActive(true);
        }

        if(catchTime + timeToPunch < Time.time)
        {
            lvl.currentTarget.GetComponent<Animator>().SetBool("lockImpact", true);
            isPunching = true;
            Time.timeScale = 0.1f;
            lvl.currentTarget.GetComponent<Animator>().SetTrigger("isPunch");
            catchTime = Time.time;
        }
        else
        {

        }
    }

    void KOevent()
    {
        if(!isDead)
        {
            GetComponent<AlwaysOnX>().enabled = false;
            gm.isFighting = false;
            ui.superPunch.SetActive(true);
            Camera.main.backgroundColor = Color.red;
            isDead = true;
        }
        
    }
}
