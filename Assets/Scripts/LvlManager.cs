﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LvlManager : MonoBehaviour
{
    private UIManager ui;
    private GameManager gm;
    public GameObject[] Target;
    public GameObject currentTarget;
    public int currentStage;

    private bool onStart = false;

    void Start()
    {
        ui = GetComponent<UIManager>();
        gm = GetComponent<GameManager>();

        currentStage = 0;
        currentTarget = Target[currentStage];
        onStart = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(onStart & Input.GetMouseButtonDown(0))
        {
            ui.fight.SetActive(false);
            EnemyEnable();
            onStart = false;
        }
    }

    public void NextTarget()
    {
        currentStage++;
        currentTarget = Target[currentStage];

        for(int i = 0; Target.Length > i; i++)
        {
            if(i == currentStage)
            {
                Target[i].GetComponent<Enemy>().enabled = true;
            }
            else
            {
                Target[i].GetComponent<Enemy>().enabled = false;
            }
        }
    }

    public void EnemyEnable()
    {
        currentTarget.GetComponent<Enemy>().enabled = true;
    }
}
