﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlwaysOnX : MonoBehaviour
{
    private Transform obj;
    public bool stayOnX, stayOnY, stayOnZ;
    public float x, y, z;

    private void Start() 
    {
        obj = transform;
    }
    void Update()
    {
        if (stayOnX)
            stayX();

        if (stayOnY)
            stayY();

        if (stayOnZ)
            stayZ();
    }

    void stayX()
    {
        if (obj.position.x > x | obj.position.x < x)
            obj.position = new Vector3(x, obj.position.y, obj.position.z);
    }
    void stayY()
    {
        if (obj.position.y > y | obj.position.y < y)
            obj.position = new Vector3(obj.position.x, y, obj.position.z);
    }
    void stayZ()
    {
        if (obj.position.z > z | obj.position.z < z)
            obj.position = new Vector3(obj.position.x, obj.position.y, z);
    }
}
