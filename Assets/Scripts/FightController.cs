﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightController : MonoBehaviour
{
    private UIManager ui;
    private LvlManager lvl;
    private GameManager gm;
    [HideInInspector]
    public Enemy enemy;
    private MoveNext moveNext;


    public float PlayerHealth;
    public float PlayerDmg;
    

    public bool isEvaded;
    public bool isEvading;
    public bool isPunching;
    public bool isSuperPunching;
    public bool getHit;
    void Start()
    {
        lvl = GetComponent<LvlManager>();
        ui = GetComponent<UIManager>();
        gm = GetComponent<GameManager>();        
        enemy = lvl.Target[0].GetComponent<Enemy>();
        moveNext = gm.GetComponent<MoveNext>();
        gm.isFighting = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (gm.isFighting)
        {
            Fighting();
        }
        else
            isPunching = false;

        if(PlayerHealth <= 0)
        {
            lose();
        }
    }

    void Fighting()
    {
        if (Time.timeScale == 1 & Input.GetMouseButtonDown(0))
        {
            isPunching = true;

        }
        else if (Time.timeScale < 1)
            isPunching = false;

        if (Input.GetMouseButtonUp(0))
        {
            isEvading = true;
            if (enemy.isPunching)
                isEvaded = true;

            Time.timeScale = 1;
            isPunching = false;
        }
    }

    Camera cam;
    GameObject wall;
    public void SuperPunch()
    {
        wall = Instantiate(gm.wall, lvl.currentTarget.transform.position + Vector3.right * 5 + Vector3.up * 1.5f, Quaternion.Euler(0, 90, 0));
        isSuperPunching = true;
        StartCoroutine("PowerfullWin");
    }

    IEnumerator PowerfullWin()
    {
        yield return new WaitForSeconds(1.3f);

        cam = Instantiate(gm.enemyCam, lvl.currentTarget.transform);

        yield return new WaitForSeconds(2f);
        
        ui.ko.SetActive(true);
        
        yield return new WaitForSeconds(2f);

        ui.ko.SetActive(false);
        Destroy(cam);
        Destroy(wall);
        lvl.currentTarget.SetActive(false);
        moveNext.MoveToNextEnemy();
    }

    public void WeakWin()
    {
        StartCoroutine("win");
    }

    IEnumerator win()
    {
        yield return new WaitForSeconds(1f);
        
        ui.ko.SetActive(true);
        
        yield return new WaitForSeconds(2f);

        ui.ko.SetActive(false);
        Destroy(cam);
        Destroy(wall);
        lvl.currentTarget.SetActive(false);
        moveNext.MoveToNextEnemy();
    }

    

    void lose()
    {
        PlayerHealth = 1;
        gm.isFighting = false;
        ui.lose.SetActive(true);
    }
}
