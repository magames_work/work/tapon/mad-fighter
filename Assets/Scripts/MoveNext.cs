﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MoveNext : MonoBehaviour
{
    private UIManager ui;
    private LvlManager lvl;
    private GameManager gm;
    private Enemy enemy;
    private FightController fc;

    public float[] enemyPointsX;
    public bool isMoving;


    void Start()
    {
        lvl = GetComponent<LvlManager>();
        ui = GetComponent<UIManager>();
        gm = GetComponent<GameManager>();
        fc = gm.GetComponent<FightController>();
        enemy = fc.enemy;
    }

    public void MoveToNextEnemy()
    {
        StartCoroutine("doMove");
    }
    IEnumerator doMove()
    {
        Camera.main.backgroundColor = new Color32(180, 240, 240, 0);
        gm.Player.GetComponent<AlwaysOnX>().enabled = false;
        isMoving = true;

        if (lvl.currentStage + 1 <= lvl.Target.Length - 1)
            gm.Player.transform.DOMoveX(enemyPointsX[lvl.currentStage + 1] - 1.5f, 5);
        else
            ui.win.SetActive(true);

        gm.Player.GetComponent<Animator>().SetBool("isWalking", true);
        lvl.currentTarget.GetComponent<Enemy>().enabled = false;

        yield return new WaitForSeconds(5);

        gm.Player.GetComponent<Animator>().SetBool("isWalking", false);
        gm.isFighting = true;
        isMoving = false;


        gm.Player.GetComponent<AlwaysOnX>().x = enemyPointsX[lvl.currentStage + 1] - 1.5f;
        gm.Player.GetComponent<AlwaysOnX>().enabled = true;

        if (lvl.currentStage + 1 == lvl.Target.Length - 1)
            ui.boss.SetActive(true);
        else
            ui.fight.SetActive(true);

        fc.PlayerHealth = ui.enemySlider.maxValue;
        enemy.health = ui.playerSlider.maxValue;

        yield return new WaitUntil(() => Input.GetMouseButtonDown(0));

        ui.fight.SetActive(false);
        ui.boss.SetActive(false);

        lvl.NextTarget();
        ui.ResetSliders();
        fc.enemy = lvl.currentTarget.GetComponent<Enemy>();
    }
}
