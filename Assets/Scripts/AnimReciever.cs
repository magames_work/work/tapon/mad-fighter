﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimReciever : MonoBehaviour
{
    private AnimManager anim;
    private GameManager gm;
    private LvlManager lvl;
    public ParticleSystem headImpact, legImpact, KOimpact;

    private void Start()
    {
        anim = FindObjectOfType<AnimManager>();
        lvl = anim.GetComponent<LvlManager>();
        gm = anim.GetComponent<GameManager>();

    }

    public void FrontHitReact()
    {
        anim.Impact(1);
        //hitFX.transform.position = new Vector3(-0.004f, 0.002f, 0.003f);

        headImpact = lvl.currentTarget.GetComponent<AnimReciever>().headImpact;
        headImpact.Play();
    }

    public void LeftHitReact()
    {
        anim.Impact(2);
        //hitFX.transform.position = new Vector3(-0.004f, 0.002f, 0.003f);
        headImpact = lvl.currentTarget.GetComponent<AnimReciever>().headImpact;
        headImpact.Play();
    }

    public void RightHitReact()
    {
        anim.Impact(3);
        //hitFX.transform.position = new Vector3(-0.004f, 0.002f, 0.003f);
        headImpact = lvl.currentTarget.GetComponent<AnimReciever>().headImpact;
        headImpact.Play();
    }

    public void UpperHitReact()
    {
        anim.Impact(4);
        //hitFX.transform.position = new Vector3(-0.004f, 0.002f, 0.003f);
        headImpact = lvl.currentTarget.GetComponent<AnimReciever>().headImpact;
        headImpact.Play();
    }

    public void LowerHitReact()
    {
        anim.Impact(5);
        //hitFX.transform.position = new Vector3(-0.005f, -0.0153f, 0.0011f);
        legImpact = lvl.currentTarget.GetComponent<AnimReciever>().legImpact;
        legImpact.Play();
    }

    public void EnemyPunch()
    {
        anim.Impact(6);
    }

    public void SetPlayerDamage()
    {
        if (!gm.GetComponent<FightController>().isEvaded)
        {
            gm.GetComponent<FightController>().PlayerHealth -= lvl.currentTarget.GetComponent<Enemy>().punchDmg;

        }
        else
        {
            gm.GetComponent<FightController>().isEvaded = false;
        }
    }

    public void SetEnemyDamage()
    {
        lvl.currentTarget.GetComponent<Enemy>().health -= gm.GetComponent<FightController>().PlayerDmg;
    }

    public void PlayerSuperPunchImpact()
    {
        anim.Impact(7);
        KOimpact = lvl.currentTarget.GetComponent<AnimReciever>().KOimpact;
        KOimpact.Play();
    }
}
