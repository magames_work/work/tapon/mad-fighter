﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private LvlManager lvl;
    private GameManager gm;

    public Slider enemySlider, playerSlider;

    public Image alert;

    public GameObject superPunch, win, lose, fight, boss, ko;

    void Start()
    {
        lvl = GetComponent<LvlManager>();
        gm = GetComponent<GameManager>();

        ResetSliders();
    }

    private void Update()
    {
        playerSlider.value = gm.GetComponent<FightController>().PlayerHealth;
        enemySlider.value = lvl.currentTarget.GetComponent<Enemy>().health;
    }

    public void ResetSliders()
    {
        playerSlider.maxValue = gm.GetComponent<FightController>().PlayerHealth;
        enemySlider.maxValue = lvl.currentTarget.GetComponent<Enemy>().health;
    }

}
