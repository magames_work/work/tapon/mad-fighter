﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SuperTouchDetect : MonoBehaviour
{
    private GameManager gm;
    private UIManager ui;
    private FightController fc;
    private LvlManager lvl;

    public float clenchTime = 5;

    public bool inZone;
    bool isSuccess;
    bool startCount;
    float catchTime;

    // Start is called before the first frame update
    void Start()
    {
        gm = FindObjectOfType<GameManager>();
        ui = gm.GetComponent<UIManager>();
        fc = gm.GetComponent<FightController>();
        lvl = gm.GetComponent<LvlManager>();
        
    }

    DG.Tweening.Core.TweenerCore<Vector3, Vector3, DG.Tweening.Plugins.Options.VectorOptions> clench;
    private void OnEnable()
    {
        transform.localScale = Vector3.one;
        clench = transform.DOScale(0.1f, clenchTime);
        startCount = true;
        catchTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            if(inZone)
            {
                fc.SuperPunch();
                ui.superPunch.SetActive(false);
                clench.Kill();
                isSuccess = true;
            }
            else
            {
                weak();
            }
        }
        if(catchTime + clenchTime < Time.time)
        {
            weak();
            clench.Kill();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        inZone = true;
        
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        inZone = false;
    }

    void weak()
    {
        fc.WeakWin();
        lvl.currentTarget.GetComponent<Animator>().SetTrigger("weakKO");
        inZone = false;
        ui.superPunch.SetActive(false);
        clench.Kill();
    }
}
